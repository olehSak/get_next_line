/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/19 04:53:13 by osak              #+#    #+#             */
/*   Updated: 2017/12/04 17:31:40 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strequ(char const *s1, char const *s2)
{
	int		z;
	size_t	u;

	if (!s1 || !s2)
		return (0);
	if (ft_strlen(s1) > ft_strlen(s2))
		u = ft_strlen(s1);
	else
		u = ft_strlen(s2);
	z = ft_memcmp(s1, s2, u);
	if (z == 0)
		return (1);
	else
		return (0);
}
