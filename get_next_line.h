/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 11:41:42 by osak              #+#    #+#             */
/*   Updated: 2018/04/17 14:59:57 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

#define BUFF_SIZE 1

#include <stdio.h>
#include <fcntl.h>
#include "libft/libft.h"
#include <zconf.h>//macfix
#include <stdlib.h>//macfix

int get_next_line(const int fd, char **line);

#endif
